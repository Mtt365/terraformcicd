provider "helm" {
  kubernetes {
    config_path = "/home/mahdi/kube-config"
  }
}


resource "null_resource" "test" {
  provisioner "local-exec" {
    command = "echo salam"
  }
}

resource "helm_release" "simpleapp" {
  name       = "simpleapp"
  chart      = "/home/mahdi/manifest/my-configs/simpleapp"
}

